package com.dailyexpense.expensetrack.providers

import android.content.ContentProvider
import android.content.ContentUris
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.net.Uri
import android.provider.BaseColumns
import com.dailyexpense.expensetrack.db.ExpenseDbHelper
import com.dailyexpense.expensetrack.providers.ExpensesContract.AUTHORITY


class ExpensesProvider : ContentProvider() {
    private var mDbHelper: SQLiteOpenHelper? = null
    private lateinit var mDatabase: SQLiteDatabase

    companion object {
        const val EXPENSES = 10
        const val EXPENSES_ID = 11
        const val CATEGORIES = 20
        const val CATEGORIES_ID = 21
        const val EXPENSES_WITH_CATEGORIES = 30
        const val EXPENSES_WITH_CATEGORIES_DATE = 31
        const val EXPENSES_WITH_CATEGORIES_DATE_RANGE = 32
        const val EXPENSES_WITH_CATEGORIES_SUM_DATE = 33
        const val EXPENSES_WITH_CATEGORIES_SUM_DATE_RANGE = 34
        private val sUriMatcher = UriMatcher(UriMatcher.NO_MATCH)

        /*
     * SELECT expenses._id, expenses.value, categories.name, expenses.date
     * FROM expenses JOIN categories
     * ON expenses.category_id = categories._id
     */
        private val BASE_SELECT_JOIN_EXPENSES_CATEGORIES_QUERY = "SELECT " + ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME + "." + BaseColumns._ID + ", " +
                ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME + "." + ExpensesContract.ExpensesColumns.Companion.VALUE + ", " +
                ExpenseDbHelper.Companion.CATEGORIES_TABLE_NAME + "." + ExpensesContract.CategoriesColumns.Companion.NAME + ", " +
                ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME + "." + ExpensesContract.ExpensesColumns.Companion.DATE + " FROM " +
                ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME + " JOIN " + ExpenseDbHelper.Companion.CATEGORIES_TABLE_NAME + " ON " +
                ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME + "." + ExpensesContract.ExpensesColumns.Companion.CATEGORY_ID + " = " +
                ExpenseDbHelper.Companion.CATEGORIES_TABLE_NAME + "." + BaseColumns._ID

        init {
            sUriMatcher.addURI(AUTHORITY, "expenses", EXPENSES)
            sUriMatcher.addURI(AUTHORITY, "expenses/#", EXPENSES_ID)
            sUriMatcher.addURI(AUTHORITY, "categories", CATEGORIES)
            sUriMatcher.addURI(AUTHORITY, "categories/#", CATEGORIES_ID)
            sUriMatcher.addURI(AUTHORITY, "expensesWithCategories",
                    EXPENSES_WITH_CATEGORIES)
            sUriMatcher.addURI(AUTHORITY, "expensesWithCategories/date",
                    EXPENSES_WITH_CATEGORIES_DATE)
            sUriMatcher.addURI(AUTHORITY, "expensesWithCategories/dateRange",
                    EXPENSES_WITH_CATEGORIES_DATE_RANGE)
            sUriMatcher.addURI(AUTHORITY, "expensesWithCategories/date/sum",
                    EXPENSES_WITH_CATEGORIES_SUM_DATE)
            sUriMatcher.addURI(AUTHORITY, "expensesWithCategories/dateRange/sum",
                    EXPENSES_WITH_CATEGORIES_SUM_DATE_RANGE)
        }
    }

    /**
     *
     *
     * Initializes the provider.
     *
     *
     * *Note*: provider is not created until a
     * [ContentResolver][android.content.ContentResolver] object tries to access it.
     *
     * @return `true` if the provider was successfully loaded, `false` otherwise
     */
    override fun onCreate(): Boolean {
        mDbHelper = ExpenseDbHelper(context)
        return true
    }

    override fun query(uri: Uri, projection: Array<String>?, selection: String?, selectionArgs: Array<String>?, sortOrder: String?): Cursor? {
        var selection = selection
        var selectionArgs = selectionArgs
        var sortOrder = sortOrder
        val cursor: Cursor
        val table: String
        val rawQuery: String
        mDatabase = mDbHelper!!.readableDatabase
        when (sUriMatcher.match(uri)) {
            CATEGORIES -> {
                table = ExpenseDbHelper.Companion.CATEGORIES_TABLE_NAME
                sortOrder = if (sortOrder == null || sortOrder.isEmpty()) ExpensesContract.Categories.DEFAULT_SORT_ORDER else sortOrder
            }
            CATEGORIES_ID -> {
                table = ExpenseDbHelper.Companion.CATEGORIES_TABLE_NAME
                // Defines selection criteria for the row to query
                selection = BaseColumns._ID + " = ?"
                selectionArgs = arrayOf(uri.lastPathSegment!!)
            }
            EXPENSES -> {
                table = ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME
                sortOrder = if (sortOrder == null || sortOrder.isEmpty()) ExpensesContract.Expenses.DEFAULT_SORT_ORDER else sortOrder
            }
            EXPENSES_ID -> {
                table = ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME
                // Defines selection criteria for the row to query
                selection = BaseColumns._ID + " = ?"
                selectionArgs = arrayOf(uri.lastPathSegment!!)
            }
            EXPENSES_WITH_CATEGORIES ->                 /*
                 * SELECT expenses._id, expenses.value, categories.name, expenses.date
                 * FROM expenses JOIN categories
                 * ON expenses.category_id = categories._id
                 */
                return mDatabase.rawQuery(BASE_SELECT_JOIN_EXPENSES_CATEGORIES_QUERY, null)
            EXPENSES_WITH_CATEGORIES_DATE -> {
                /*
                 * SELECT expenses._id, expenses.value, categories.name, expenses.date
                 * FROM expenses JOIN categories
                 * ON expenses.category_id = categories._id
                 * WHERE expense.date = ?
                 */rawQuery = BASE_SELECT_JOIN_EXPENSES_CATEGORIES_QUERY + " WHERE " +
                        ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME + "." + ExpensesContract.ExpensesColumns.Companion.DATE + " = ?"
                return mDatabase.rawQuery(rawQuery, selectionArgs)
            }
            EXPENSES_WITH_CATEGORIES_SUM_DATE -> {
                /*
                 * SELECT SUM(expenses.value) as values_sum
                 * FROM expenses WHERE expenses.date = ?
                 */rawQuery = "SELECT SUM(" + ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME + "." + ExpensesContract.ExpensesColumns.Companion.VALUE + ") as " +
                        ExpensesContract.Expenses.VALUES_SUM + " FROM " + ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME +
                        " WHERE " + ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME + "." + ExpensesContract.ExpensesColumns.Companion.DATE + " = ?"
                return mDatabase.rawQuery(rawQuery, selectionArgs)
            }
            EXPENSES_WITH_CATEGORIES_DATE_RANGE -> {
                /*
                 * SELECT expenses._id, expenses.value, categories.name, expenses.date
                 * FROM expenses JOIN categories
                 * ON expenses.category_id = categories._id
                 * WHERE expense.date BETWEEN ? AND ?
                 */rawQuery = BASE_SELECT_JOIN_EXPENSES_CATEGORIES_QUERY + " WHERE " +
                        ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME + "." + ExpensesContract.ExpensesColumns.Companion.DATE + " BETWEEN ? AND ?"
                return mDatabase.rawQuery(rawQuery, selectionArgs)
            }
            EXPENSES_WITH_CATEGORIES_SUM_DATE_RANGE -> {
                /*
                 * SELECT SUM(expenses.value) as values_sum
                 * FROM expenses WHERE expense.date BETWEEN ? AND ?
                 */rawQuery = "SELECT SUM(" + ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME + "." + ExpensesContract.ExpensesColumns.Companion.VALUE + ") as " +
                        ExpensesContract.Expenses.VALUES_SUM + " FROM " + ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME +
                        " WHERE " + ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME + "." + ExpensesContract.ExpensesColumns.Companion.DATE + " BETWEEN ? AND ?"
                return mDatabase.rawQuery(rawQuery, selectionArgs)
            }
            else -> throw IllegalArgumentException("Unknown Uri provided.")
        }
        cursor = mDatabase.query(
                table,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        )
        return cursor
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        val table: String
        val contentUri: Uri?
        when (sUriMatcher.match(uri)) {
            CATEGORIES -> {
                table = ExpenseDbHelper.Companion.CATEGORIES_TABLE_NAME
                contentUri = ExpensesContract.Categories.CONTENT_URI
            }
            EXPENSES -> {
                table = ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME
                contentUri = ExpensesContract.Expenses.CONTENT_URI
            }
            CATEGORIES_ID, EXPENSES_ID -> throw UnsupportedOperationException("Inserting rows with specified IDs is forbidden.")
            EXPENSES_WITH_CATEGORIES, EXPENSES_WITH_CATEGORIES_DATE, EXPENSES_WITH_CATEGORIES_DATE_RANGE, EXPENSES_WITH_CATEGORIES_SUM_DATE, EXPENSES_WITH_CATEGORIES_SUM_DATE_RANGE -> throw UnsupportedOperationException("Modifying joined results is forbidden.")
            else -> throw IllegalArgumentException("Unknown Uri provided.")
        }
        mDatabase = mDbHelper!!.writableDatabase
        val newRowID = mDatabase.insert(
                table,
                null,
                values
        )
        val newItemUri = ContentUris.withAppendedId(contentUri, newRowID)
        return if (newRowID < 1) null else newItemUri
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        var selection = selection
        var selectionArgs = selectionArgs
        val table: String
        when (sUriMatcher.match(uri)) {
            CATEGORIES_ID -> {
                table = ExpenseDbHelper.Companion.CATEGORIES_TABLE_NAME
                // Defines selection criteria for the row to delete
                selection = BaseColumns._ID + " = ?"
                selectionArgs = arrayOf(uri.lastPathSegment!!)
            }
            EXPENSES -> table = ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME
            EXPENSES_ID -> {
                table = ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME
                // Defines selection criteria for the row to delete
                selection = BaseColumns._ID + " = ?"
                selectionArgs = arrayOf(uri.lastPathSegment!!)
            }
            CATEGORIES -> throw UnsupportedOperationException("Removing multiple rows from the table is forbidden.")
            EXPENSES_WITH_CATEGORIES, EXPENSES_WITH_CATEGORIES_DATE, EXPENSES_WITH_CATEGORIES_DATE_RANGE, EXPENSES_WITH_CATEGORIES_SUM_DATE, EXPENSES_WITH_CATEGORIES_SUM_DATE_RANGE -> throw UnsupportedOperationException("Modifying joined results is forbidden.")
            else -> throw IllegalArgumentException("Unknown Uri provided.")
        }
        mDatabase = mDbHelper!!.writableDatabase
        return mDatabase.delete(
                table,
                selection,
                selectionArgs
        )
    }

    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
        var selection = selection
        var selectionArgs = selectionArgs
        val table: String
        when (sUriMatcher.match(uri)) {
            CATEGORIES_ID -> {
                table = ExpenseDbHelper.Companion.CATEGORIES_TABLE_NAME
                // Defines selection criteria for the row to delete
                selection = BaseColumns._ID + " = ?"
                selectionArgs = arrayOf(uri.lastPathSegment!!)
            }
            EXPENSES_ID -> {
                table = ExpenseDbHelper.Companion.EXPENSES_TABLE_NAME
                // Defines selection criteria for the row to delete
                selection = BaseColumns._ID + " = ?"
                selectionArgs = arrayOf(uri.lastPathSegment!!)
            }
            CATEGORIES, EXPENSES -> throw UnsupportedOperationException("Updating multiple table rows is forbidden.")
            EXPENSES_WITH_CATEGORIES, EXPENSES_WITH_CATEGORIES_DATE, EXPENSES_WITH_CATEGORIES_DATE_RANGE, EXPENSES_WITH_CATEGORIES_SUM_DATE, EXPENSES_WITH_CATEGORIES_SUM_DATE_RANGE -> throw UnsupportedOperationException("Modifying joined results is forbidden.")
            else -> throw IllegalArgumentException("Unknown Uri provided.")
        }
        mDatabase = mDbHelper!!.writableDatabase
        return mDatabase.update(
                table,
                values,
                selection,
                selectionArgs
        )
    }

    override fun getType(uri: Uri): String? {
        val match = sUriMatcher.match(uri)
        return when (match) {
            CATEGORIES -> ExpensesContract.Categories.CONTENT_TYPE
            CATEGORIES_ID -> ExpensesContract.Categories.CONTENT_ITEM_TYPE
            EXPENSES -> ExpensesContract.Expenses.CONTENT_TYPE
            EXPENSES_ID -> ExpensesContract.Expenses.CONTENT_ITEM_TYPE
            EXPENSES_WITH_CATEGORIES, EXPENSES_WITH_CATEGORIES_DATE, EXPENSES_WITH_CATEGORIES_DATE_RANGE, EXPENSES_WITH_CATEGORIES_SUM_DATE, EXPENSES_WITH_CATEGORIES_SUM_DATE_RANGE -> ExpensesContract.ExpensesWithCategories.CONTENT_TYPE
            else -> null
        }
    }
}