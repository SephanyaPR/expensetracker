package com.dailyexpense.expensetrack.providers

import android.net.Uri
import android.provider.BaseColumns

object ExpensesContract {
    /**
     * The authority for the expenses provider
     */
    const val AUTHORITY = "com.expensetracer.provider"

    /**
     * The content:// style URI for expenses provider
     */
    val AUTHORITY_URI = Uri.parse("content://$AUTHORITY")

    object Categories : BaseColumns, CategoriesColumns {
        /**
         * The content:// style URI for this table
         */
        val CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "categories")

        /**
         * The MIME type of [.CONTENT_URI] providing a directory of categories.
         */
        const val CONTENT_TYPE = "vnd.android.cursor.dir/vnd.expensetracer.provider.expense_category"

        /**
         * The MIME type of a [.CONTENT_URI] sub-directory of a single category.
         */
        const val CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.expensetracer.provider.expense_category"

        /**
         * Sort by ascending order of _id column (the order as items were added).
         */
        const val DEFAULT_SORT_ORDER = BaseColumns._ID + " ASC"
    }

    object Expenses : BaseColumns, ExpensesColumns {
        /**
         * The content:// style URI for this table
         */
        val CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "expenses")

        /**
         * The MIME type of [.CONTENT_URI] providing a directory of expenses.
         */
        const val CONTENT_TYPE = "vnd.android.cursor.dir/vnd.expensetracer.provider.expense"

        /**
         * The MIME type of a [.CONTENT_URI] sub-directory of a single expense.
         */
        const val CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.expensetracer.provider.expense"

        /**
         * Sort by descending order of date (the most recent items are at the end).
         */
        const val DEFAULT_SORT_ORDER = ExpensesColumns.DATE + " ASC"

        /**
         * Expense sum value column name to return for joined tables
         */
        const val VALUES_SUM = "values_sum"
    }

    object ExpensesWithCategories : BaseColumns {
        /**
         * The content:// style URI for this table.
         */
        val CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "expensesWithCategories")

        /**
         * The MIME type of [.CONTENT_URI] providing a directory of expenses with categories.
         */
        const val CONTENT_TYPE = "vnd.android.cursor.dir/vnd.expensetracer.provider.expense_with_category"
        /**
         * The MIME type of a [.CONTENT_URI] sub-directory of a single expense with a category.
         */
        //        public static final String CONTENT_ITEM_TYPE =
        //                "vnd.android.cursor.item/vnd.ematiyuk.expensetracer.provider.expense_with_category";
        /**
         * The content:// style URI for this joined table to filter items by a specific date.
         */
        val DATE_CONTENT_URI = Uri.withAppendedPath(CONTENT_URI, "date")

        /**
         * The content:// style URI for this joined table to filter items by a specific date range.
         */
        val DATE_RANGE_CONTENT_URI = Uri.withAppendedPath(CONTENT_URI, "dateRange")

        /**
         * The content:// style URI for getting sum of expense values
         * for this joined table by "date" filter.
         */
        val SUM_DATE_CONTENT_URI = Uri.withAppendedPath(DATE_CONTENT_URI, "sum")

        /**
         * The content:// style URI for getting sum of expense values
         * for this joined table by "date range" filter.
         */
        val SUM_DATE_RANGE_CONTENT_URI = Uri.withAppendedPath(DATE_RANGE_CONTENT_URI, "sum")
    }

    interface CategoriesColumns {
        companion object {
            const val NAME = "name"
        }
    }

    interface ExpensesColumns {
        companion object {
            const val VALUE = "value"
            const val DATE = "date"
            const val CATEGORY_ID = "category_id"
        }
    }
}