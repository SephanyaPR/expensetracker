package com.dailyexpense.expensetrack.activity

import android.os.Bundle
import com.dailyexpense.expensetrack.R
import com.dailyexpense.expensetrack.fragment.SettingsFragment

class SettingsActivity : BaseFragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        insertFragment(SettingsFragment())
        setTitle(R.string.nav_settings)
        setupActionBar()
    }

    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }
}