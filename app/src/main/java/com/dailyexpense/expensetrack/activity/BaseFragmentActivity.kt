package com.dailyexpense.expensetrack.activity

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.dailyexpense.expensetrack.R

abstract class BaseFragmentActivity : AppCompatActivity() {
    protected var mToolbar: Toolbar? = null

    @get:LayoutRes
    protected open val layoutResId: Int
        protected get() = R.layout.activity_base

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)

        // Set a Toolbar to replace the ActionBar
        mToolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(mToolbar)
    }

    protected fun insertFragment(fragment: Fragment?) {
        // Insert the fragment by replacing any existing fragment
        val fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment!!)
                .commit()
    }
}