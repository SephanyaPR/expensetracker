package com.dailyexpense.expensetrack.activity

import android.os.Bundle
import com.dailyexpense.expensetrack.fragment.ExpenseEditFragment

class ExpenseEditActivity : BaseFragmentActivity() {
    /* Important: use onCreate(Bundle savedInstanceState)
     * instead of onCreate(Bundle savedInstanceState, PersistableBundle persistentState) */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        insertFragment(ExpenseEditFragment())
        setupActionBar()
    }

    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }
}