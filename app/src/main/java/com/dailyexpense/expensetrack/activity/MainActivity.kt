package com.dailyexpense.expensetrack.activity

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.dailyexpense.expensetrack.R
import com.dailyexpense.expensetrack.fragment.CategoryFragment
import com.dailyexpense.expensetrack.fragment.ReportFragment
import com.dailyexpense.expensetrack.fragment.TodayFragment
import com.google.android.material.navigation.NavigationView

class MainActivity : BaseFragmentActivity() {
    private var mDrawerLayout: DrawerLayout? = null
    private var mNavDrawer: NavigationView? = null
    private var mDrawerToggle: ActionBarDrawerToggle? = null

    @get:LayoutRes
    protected override val layoutResId: Int
        protected get() = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDrawerLayout = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        mNavDrawer = findViewById<View>(R.id.nav_drawer) as NavigationView
        mDrawerToggle = setupDrawerToggle()

        // Tie DrawerLayout events to the ActionBarToggle
        mDrawerLayout!!.addDrawerListener(mDrawerToggle!!)

        // Setup drawer view
        setupDrawerContent(mNavDrawer)

        // Select TodayFragment on app start by default
        loadTodayFragment()
    }

    override fun onPause() {
        super.onPause()
        closeNavigationDrawer()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        return if (mDrawerToggle!!.onOptionsItemSelected(item)) {
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle!!.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        // Pass any configuration change to the drawer toggle
        mDrawerToggle!!.onConfigurationChanged(newConfig)
    }

    override fun onBackPressed() {
        if (!closeNavigationDrawer()) {
            val currentFragment = supportFragmentManager
                    .findFragmentById(R.id.content_frame)
            if (currentFragment !is TodayFragment) {
                loadTodayFragment()
            } else {
                // If current fragment is TodayFragment then exit
                super.onBackPressed()
            }
        }
    }

    private fun setupDrawerToggle(): ActionBarDrawerToggle {
        return ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.drawer_open, R.string.drawer_close)
    }

    private fun setupDrawerContent(navigationView: NavigationView?) {
        navigationView!!.setNavigationItemSelectedListener { menuItem ->
            selectDrawerItem(menuItem)
            true
        }
    }

    private fun selectDrawerItem(menuItem: MenuItem) {
        closeNavigationDrawer()
        when (menuItem.itemId) {
            R.id.nav_today -> loadFragment(TodayFragment::class.java, menuItem.itemId, menuItem.title)
            R.id.nav_report -> loadFragment(ReportFragment::class.java, menuItem.itemId, menuItem.title)
            R.id.nav_categories -> loadFragment(CategoryFragment::class.java, menuItem.itemId, menuItem.title)
            R.id.nav_settings -> startActivity(Intent(this@MainActivity, SettingsActivity::class.java))
            else -> loadFragment(TodayFragment::class.java, menuItem.itemId, menuItem.title)
        }
    }

    private fun closeNavigationDrawer(): Boolean {
        val drawerIsOpen = mDrawerLayout!!.isDrawerOpen(GravityCompat.START)
        if (drawerIsOpen) {
            mDrawerLayout!!.closeDrawer(GravityCompat.START)
        }
        return drawerIsOpen
    }

    fun hideNavigationBar() {
        closeNavigationDrawer()
    }

    private fun loadFragment(fragmentClass: Class<*>, @IdRes navDrawerCheckedItemId: Int,
                             toolbarTitle: CharSequence) {
        var fragment: Fragment? = null
        try {
            fragment = fragmentClass.newInstance() as Fragment
        } catch (e: Exception) {
            e.printStackTrace()
        }
        insertFragment(fragment)

        // Highlight the selected item
        mNavDrawer!!.setCheckedItem(navDrawerCheckedItemId)
        // Set action bar title
        title = toolbarTitle
    }

    private fun loadTodayFragment() {
        loadFragment(TodayFragment::class.java, R.id.nav_today,
                resources.getString(R.string.nav_today))
    }
}