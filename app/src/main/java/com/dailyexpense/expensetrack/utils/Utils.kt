package com.dailyexpense.expensetrack.utils

import android.content.Context
import android.text.format.DateFormat
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object Utils {
    fun getSystemFormatDateString(context: Context?, date: Date?): String {
        val dateFormat = DateFormat.getDateFormat(context)
        return dateFormat.format(date)
    }

    fun getSystemFormatDateString(context: Context?, dateString: String): String {
        val dateFormat = DateFormat.getDateFormat(context)
        return dateFormat.format(stringToDate(dateString))
    }

    fun getDateString(date: Date?): String {
        val dateFormat = SimpleDateFormat("MM/dd/yy", Locale.US)
        return try {
            dateFormat.format(date)
        } catch (pe: Exception) {
            pe.printStackTrace()
            "no_date"
        }
    }

    private fun stringToDate(dateString: String): Date? {
        val dateFormat = SimpleDateFormat("MM/dd/yy", Locale.US)
        return try {
            dateFormat.parse(dateString)
        } catch (pe: ParseException) {
            pe.printStackTrace()
            null
        }
    }

    fun formatToCurrency(value: Float): String {
        val numberFormat = NumberFormat.getNumberInstance()
        numberFormat.maximumFractionDigits = 2
        numberFormat.minimumFractionDigits = 2
        return numberFormat.format(value.toDouble())
    }
}