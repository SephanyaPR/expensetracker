package com.dailyexpense.expensetrack.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dailyexpense.expensetrack.R
import com.dailyexpense.expensetrack.providers.ExpensesContract.CategoriesColumns
import com.dailyexpense.expensetrack.providers.ExpensesContract.ExpensesColumns
import com.dailyexpense.expensetrack.utils.Utils
import com.twotoasters.sectioncursoradapter.SectionCursorAdapter

class SectionExpenseAdapter(context: Context?) : SectionCursorAdapter(context, null, 0) {
    private var mCurrency: String? = null
    fun setCurrency(currency: String?) {
        mCurrency = currency
        notifyDataSetChanged()
    }

    @SuppressLint("RestrictedApi")
    override fun getSectionFromCursor(cursor: Cursor): Any {
        val dateStr = cursor.getString(cursor.getColumnIndexOrThrow(ExpensesColumns.Companion.DATE))
        return Utils.getSystemFormatDateString(mContext, dateStr)
    }

    override fun newSectionView(context: Context, item: Any, parent: ViewGroup): View {
        return layoutInflater.inflate(R.layout.expense_report_section_header, parent, false)
    }

    override fun bindSectionView(convertView: View, context: Context, position: Int, item: Any) {
        (convertView as TextView).text = item as String
    }

    override fun newItemView(context: Context, cursor: Cursor, parent: ViewGroup): View {
        return layoutInflater.inflate(R.layout.expense_list_item, parent, false)
    }

    override fun bindItemView(convertView: View, context: Context, cursor: Cursor) {
        // Find fields to populate in inflated template
        val tvExpenseValue = convertView.findViewById<View>(R.id.expense_value_text_view) as TextView
        val tvExpenseCurrency = convertView.findViewById<View>(R.id.expense_currency_text_view) as TextView
        val tvExpenseCatName = convertView.findViewById<View>(R.id.expense_category_name_text_view) as TextView

        // Extract values from cursor
        val expValue = cursor.getFloat(cursor.getColumnIndexOrThrow(ExpensesColumns.Companion.VALUE))
        val categoryName = cursor.getString(cursor.getColumnIndexOrThrow(CategoriesColumns.Companion.NAME))

        // Populate views with extracted values
        tvExpenseValue.text = Utils.formatToCurrency(expValue)
        tvExpenseCatName.text = categoryName
        tvExpenseCurrency.text = mCurrency
    }
}