package com.dailyexpense.expensetrack.adapters

import android.content.Context
import android.database.Cursor
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cursoradapter.widget.CursorAdapter
import com.dailyexpense.expensetrack.R
import com.dailyexpense.expensetrack.providers.ExpensesContract.CategoriesColumns
import com.dailyexpense.expensetrack.providers.ExpensesContract.ExpensesColumns
import com.dailyexpense.expensetrack.utils.Utils

class SimpleExpenseAdapter(context: Context?) : CursorAdapter(context, null, 0) {
    private var mCurrency: String? = null
    fun setCurrency(currency: String?) {
        mCurrency = currency
        notifyDataSetChanged()
    }

    // The newView method is used to inflate a new view and return it
    override fun newView(context: Context, cursor: Cursor, parent: ViewGroup): View {
        return LayoutInflater.from(context).inflate(R.layout.expense_list_item, parent, false)
    }

    // The bindView method is used to bind all data to a given view
    override fun bindView(view: View, context: Context, cursor: Cursor) {
        // Find fields to populate in inflated template
        val tvExpenseValue = view.findViewById<View>(R.id.expense_value_text_view) as TextView
        val tvExpenseCurrency = view.findViewById<View>(R.id.expense_currency_text_view) as TextView
        val tvExpenseCatName = view.findViewById<View>(R.id.expense_category_name_text_view) as TextView

        // Extract values from cursor
        val expValue = cursor.getFloat(cursor.getColumnIndexOrThrow(ExpensesColumns.Companion.VALUE))
        val categoryName = cursor.getString(cursor.getColumnIndexOrThrow(CategoriesColumns.Companion.NAME))

        // Populate views with extracted values
        tvExpenseValue.text = Utils.formatToCurrency(expValue)
        tvExpenseCatName.text = categoryName
        tvExpenseCurrency.text = mCurrency
    }
}