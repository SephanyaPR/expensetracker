package com.dailyexpense.expensetrack.fragment

import android.content.ContentUris
import android.content.ContentValues
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.BaseColumns
import android.view.*
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.widget.AppCompatSpinner
import androidx.fragment.app.Fragment
import androidx.loader.app.LoaderManager
import androidx.loader.content.CursorLoader
import androidx.loader.content.Loader
import com.dailyexpense.expensetrack.R
import com.dailyexpense.expensetrack.providers.ExpensesContract
import com.dailyexpense.expensetrack.utils.Utils
import java.util.*

class ExpenseEditFragment : Fragment(), LoaderManager.LoaderCallbacks<Cursor?> {
    private var mExpValueEditText: EditText? = null
    private var mCategorySpinner: AppCompatSpinner? = null
    private var mAdapter: SimpleCursorAdapter? = null
    private var mCatProgressBar: View? = null
    private var mExtraValue: Long = 0
    private var mExpenseCategoryId: Long = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_expense_edit, container, false)
        mExpValueEditText = rootView.findViewById<View>(R.id.expense_value_edit_text) as EditText
        mCatProgressBar = rootView.findViewById(R.id.cat_select_progress_bar)
        mCategorySpinner = rootView.findViewById<View>(R.id.category_choose_spinner) as AppCompatSpinner
        setEditTextDefaultValue()

        // Set listener on Done (submit) button on keyboard clicked
        mExpValueEditText!!.setOnKeyListener(View.OnKeyListener { view, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                checkValueFieldForIncorrectInput()
                return@OnKeyListener true
            }
            false
        })
        mCategorySpinner!!.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, pos: Int, id: Long) {
                mExpenseCategoryId = id
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mAdapter = SimpleCursorAdapter(activity,
                android.R.layout.simple_spinner_item,
                null, arrayOf(ExpensesContract.CategoriesColumns.Companion.NAME), intArrayOf(android.R.id.text1),
                0)
        // Specify the layout to use when the list of choices appears
        mAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Apply the adapter to the spinner
        mCategorySpinner!!.adapter = mAdapter
        mExtraValue = activity!!.intent.getLongExtra(EXTRA_EDIT_EXPENSE, -1)
        // Create a new expense
        if (mExtraValue < 1) {
            activity!!.setTitle(R.string.add_expense)
            loadCategories()

            // Edit existing expense
        } else {
            activity!!.setTitle(R.string.edit_expense)
            loadExpenseData()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_expense_edit, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.done_expense_edit_menu_item -> {
                if (checkForIncorrectInput()) {
                    // Create a new expense
                    if (mExtraValue < 1) {
                        insertNewExpense()

                        // Edit existing expense
                    } else {
                        updateExpense(mExtraValue)
                    }
                    activity!!.finish()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun checkForIncorrectInput(): Boolean {
        if (!checkValueFieldForIncorrectInput()) {
            mExpValueEditText!!.selectAll()
            return false
        }
        // Future check of other fields
        return true
    }

    private fun checkValueFieldForIncorrectInput(): Boolean {
        val etValue = mExpValueEditText!!.text.toString()
        try {
            if (etValue.length == 0) {
                mExpValueEditText!!.error = resources.getString(R.string.error_empty_field)
                return false
            } else if (etValue.toFloat() == 0.00f) {
                mExpValueEditText!!.error = resources.getString(R.string.error_zero_value)
                return false
            }
        } catch (e: Exception) {
            mExpValueEditText!!.error = resources.getString(R.string.error_incorrect_input)
            return false
        }
        return true
    }

    private fun loadCategories() {
        // Show the progress bar next to category spinner
        mCatProgressBar!!.visibility = View.VISIBLE
        loaderManager.initLoader<Cursor>(CATEGORIES_LOADER_ID, null, this)
    }

    private fun loadExpenseData() {
        loaderManager.initLoader<Cursor>(EXPENSE_LOADER_ID, null, this)
        loadCategories()
    }

    private fun setEditTextDefaultValue() {
        mExpValueEditText!!.setText(0.toString())
        mExpValueEditText!!.selectAll()
    }

    override fun onCreateLoader(id: Int, args: Bundle?): CursorLoader {
        var projectionFields: Array<String>? = null
        var uri: Uri? = null
        when (id) {
            EXPENSE_LOADER_ID -> {
                projectionFields = arrayOf(
                        BaseColumns._ID,
                        ExpensesContract.ExpensesColumns.Companion.VALUE,
                        ExpensesContract.ExpensesColumns.Companion.CATEGORY_ID
                )
                uri = ContentUris.withAppendedId(ExpensesContract.Expenses.CONTENT_URI, mExtraValue)
            }
            CATEGORIES_LOADER_ID -> {
                projectionFields = arrayOf(
                        BaseColumns._ID,
                        ExpensesContract.CategoriesColumns.Companion.NAME
                )
                uri = ExpensesContract.Categories.CONTENT_URI
            }
        }
        return CursorLoader(activity!!,
                uri!!,
                projectionFields,
                null,
                null,
                null
        )
    }

    override fun onLoadFinished(loader: Loader<Cursor?>, data: Cursor?) {
        when (loader.id) {
            EXPENSE_LOADER_ID -> {
                val expenseValueIndex = data!!.getColumnIndex(ExpensesContract.ExpensesColumns.Companion.VALUE)
                val expenseCategoryIdIndex = data.getColumnIndex(ExpensesContract.ExpensesColumns.Companion.CATEGORY_ID)
                data.moveToFirst()
                mExpenseCategoryId = data.getLong(expenseCategoryIdIndex)
                updateSpinnerSelection()
                mExpValueEditText!!.setText(data.getFloat(expenseValueIndex).toString())
                mExpValueEditText!!.selectAll()
            }
            CATEGORIES_LOADER_ID -> {
                // Hide the progress bar next to category spinner
                mCatProgressBar!!.visibility = View.GONE
                if (null == data || data.count < 1) {
                    mExpenseCategoryId = -1
                    // Fill the spinner with default values
                    val defaultItems = ArrayList<String>()
                    defaultItems.add(resources.getString(R.string.no_categories_string))
                    val tempAdapter = ArrayAdapter(activity!!,
                            android.R.layout.simple_spinner_item,
                            defaultItems)
                    mCategorySpinner!!.adapter = tempAdapter
                    // Disable the spinner
                    mCategorySpinner!!.isEnabled = false
                } else {
                    // Set the original adapter
                    mCategorySpinner!!.adapter = mAdapter
                    // Update spinner data
                    mAdapter!!.swapCursor(data)
                    // Enable the spinner
                    mCategorySpinner!!.isEnabled = true
                    updateSpinnerSelection()
                }
            }
        }
    }

    override fun onLoaderReset(loader: Loader<Cursor?>) {
        when (loader.id) {
            EXPENSE_LOADER_ID -> {
                mExpenseCategoryId = -1
                setEditTextDefaultValue()
            }
            CATEGORIES_LOADER_ID -> mAdapter!!.swapCursor(null)
        }
    }

    private fun updateSpinnerSelection() {
        mCategorySpinner!!.setSelection(0)
        for (pos in 0 until mAdapter!!.count) {
            if (mAdapter!!.getItemId(pos) == mExpenseCategoryId) {
                // Set spinner item selected according to the value from db
                mCategorySpinner!!.setSelection(pos)
                break
            }
        }
    }

    private fun insertNewExpense() {
        val insertValues = ContentValues()
        insertValues.put(ExpensesContract.ExpensesColumns.Companion.VALUE, mExpValueEditText!!.text.toString().toFloat())
        insertValues.put(ExpensesContract.ExpensesColumns.Companion.DATE, Utils.getDateString(Date())) // Put current date (today)
        insertValues.put(ExpensesContract.ExpensesColumns.Companion.CATEGORY_ID, mExpenseCategoryId)
        activity!!.contentResolver.insert(
                ExpensesContract.Expenses.CONTENT_URI,
                insertValues
        )
        Toast.makeText(activity,
                resources.getString(R.string.expense_added),
                Toast.LENGTH_SHORT).show()
    }

    private fun updateExpense(id: Long) {
        val updateValues = ContentValues()
        updateValues.put(ExpensesContract.ExpensesColumns.Companion.VALUE, mExpValueEditText!!.text.toString().toFloat())
        updateValues.put(ExpensesContract.ExpensesColumns.Companion.CATEGORY_ID, mExpenseCategoryId)
        val expenseUri = ContentUris.withAppendedId(ExpensesContract.Expenses.CONTENT_URI, id)
        activity!!.contentResolver.update(
                expenseUri,
                updateValues,
                null,
                null
        )
        Toast.makeText(activity,
                resources.getString(R.string.expense_updated),
                Toast.LENGTH_SHORT).show()
    }

    companion object {
        const val EXTRA_EDIT_EXPENSE = "com.expensetracer.edit_expense"
        private const val EXPENSE_LOADER_ID = 1
        private const val CATEGORIES_LOADER_ID = 0
    }
}