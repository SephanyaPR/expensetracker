package com.dailyexpense.expensetrack.fragment

import android.content.ContentUris
import android.content.ContentValues
import android.database.Cursor
import android.os.Bundle
import android.provider.BaseColumns
import android.view.*
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.loader.app.LoaderManager
import androidx.loader.content.CursorLoader
import androidx.loader.content.Loader
import com.dailyexpense.expensetrack.R
import com.dailyexpense.expensetrack.providers.ExpensesContract
import com.dailyexpense.expensetrack.providers.ExpensesContract.CategoriesColumns

class CategoryEditFragment : Fragment(), LoaderManager.LoaderCallbacks<Cursor> {
    private var mCatNameEditText: EditText? = null
    private var mExtraValue: Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_category_edit, container, false)
        mCatNameEditText = rootView.findViewById<View>(R.id.category_name_edit_text) as EditText

        // Set listener on Done (submit) button on keyboard clicked
        mCatNameEditText!!.setOnKeyListener(View.OnKeyListener { view, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                checkEditTextForEmptyField(mCatNameEditText)
                return@OnKeyListener true
            }
            false
        })
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mExtraValue = activity!!.intent.getLongExtra(EXTRA_EDIT_CATEGORY, -1)
        // Create a new category
        if (mExtraValue < 1) {
            activity!!.setTitle(R.string.add_category)

            // Edit existing category
        } else {
            activity!!.setTitle(R.string.edit_category)
            setCategoryData()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_category_edit, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.done_category_edit_menu_item -> {
                if (checkEditTextForEmptyField(mCatNameEditText)) {
                    // Create a new category
                    if (mExtraValue < 1) {
                        insertNewCategory()

                        // Edit existing category
                    } else {
                        updateCategory(mExtraValue)
                    }
                    activity!!.finish()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun checkEditTextForEmptyField(editText: EditText?): Boolean {
        val inputText = editText!!.text.toString().trim { it <= ' ' }
        return if (inputText.length == 0) {
            editText.error = resources.getString(R.string.error_empty_field)
            mCatNameEditText!!.selectAll()
            false
        } else {
            true
        }
    }

    private fun setCategoryData() {
        loaderManager.initLoader(0, null, this)
    }

    override fun onCreateLoader(id: Int, args: Bundle?): CursorLoader {
        val projectionFields = arrayOf(
                BaseColumns._ID,
                CategoriesColumns.Companion.NAME
        )
        val singleCategoryUri = ContentUris.withAppendedId(ExpensesContract.Categories.CONTENT_URI, mExtraValue)
        return CursorLoader(activity!!,
                singleCategoryUri,
                projectionFields,
                null,
                null,
                null
        )
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor) {
        val categoryNameIndex = data.getColumnIndex(CategoriesColumns.Companion.NAME)
        data.moveToFirst()
        val categoryName = data.getString(categoryNameIndex)
        mCatNameEditText!!.setText(categoryName)
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {
        mCatNameEditText!!.setText("")
    }

    private fun insertNewCategory() {
        val insertValues = ContentValues()
        insertValues.put(CategoriesColumns.Companion.NAME, mCatNameEditText!!.text.toString())
        activity!!.contentResolver.insert(
                ExpensesContract.Categories.CONTENT_URI,
                insertValues
        )
        Toast.makeText(activity,
                resources.getString(R.string.category_added),
                Toast.LENGTH_SHORT).show()
    }

    private fun updateCategory(id: Long) {
        val updateValues = ContentValues()
        updateValues.put(CategoriesColumns.Companion.NAME, mCatNameEditText!!.text.toString())
        val categoryUri = ContentUris.withAppendedId(ExpensesContract.Categories.CONTENT_URI, id)
        activity!!.contentResolver.update(
                categoryUri,
                updateValues,
                null,
                null
        )
        Toast.makeText(activity,
                resources.getString(R.string.category_updated),
                Toast.LENGTH_SHORT).show()
    }

    companion object {
        const val EXTRA_EDIT_CATEGORY = "com.expensetracer.edit_category"
    }
}