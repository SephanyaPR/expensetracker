package com.dailyexpense.expensetrack.fragment

import android.content.ContentUris
import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.provider.BaseColumns
import android.view.*
import android.view.ContextMenu.ContextMenuInfo
import android.widget.AdapterView.AdapterContextMenuInfo
import android.widget.AdapterView.OnItemClickListener
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.cursoradapter.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import androidx.loader.app.LoaderManager
import androidx.loader.content.CursorLoader
import androidx.loader.content.Loader
import com.dailyexpense.expensetrack.R
import com.dailyexpense.expensetrack.activity.CategoryEditActivity
import com.dailyexpense.expensetrack.providers.ExpensesContract


class CategoryFragment : Fragment(), LoaderManager.LoaderCallbacks<Cursor> {
    private var mCategoriesView: ListView? = null
    private var mAdapter: SimpleCursorAdapter? = null
    private var mProgressBar: View? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_category, container, false)
        mCategoriesView = rootView.findViewById<View>(R.id.categories_list_view) as ListView
        mProgressBar = rootView.findViewById(R.id.categories_progress_bar)
        mCategoriesView!!.emptyView = rootView.findViewById(R.id.categories_empty_list_view)
        mCategoriesView!!.onItemClickListener = OnItemClickListener { parent, view, position, id -> prepareCategoryToEdit(id) }
        rootView.findViewById<View>(R.id.add_category_button_if_empty_list).setOnClickListener { prepareCategoryToCreate() }
        registerForContextMenu(mCategoriesView!!)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mAdapter = SimpleCursorAdapter(activity,
                R.layout.category_list_item, null, arrayOf(ExpensesContract.CategoriesColumns.Companion.NAME), intArrayOf(R.id.category_name_list_item), 0)
        mCategoriesView!!.adapter = mAdapter

        // Initialize the CursorLoader
        loaderManager.initLoader(0, null, this)
    }

    override fun onResume() {
        super.onResume()
        reloadCategoryList()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_category, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.new_category_menu_item -> {
                prepareCategoryToCreate()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        activity!!.menuInflater.inflate(R.menu.category_list_item_context, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as AdapterContextMenuInfo
        return when (item.itemId) {
            R.id.delete_category_menu_item -> {
                deleteCategory(info.id)
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        val projectionFields = arrayOf(
                BaseColumns._ID,
                ExpensesContract.CategoriesColumns.Companion.NAME
        )
        return CursorLoader(activity!!,
                ExpensesContract.Categories.CONTENT_URI,
                projectionFields,
                null,
                null,
                null
        )
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor) {
        // Hide the progress bar
        mProgressBar!!.visibility = View.GONE
        mAdapter!!.swapCursor(data)
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {
        mAdapter!!.swapCursor(null)
    }

    private fun reloadCategoryList() {
        // Show the progress bar
        mProgressBar!!.visibility = View.VISIBLE
        // Reload data by restarting the cursor loader
        loaderManager.restartLoader(0, null, this)
    }

    private fun deleteSingleCategory(categoryId: Long): Int {
        val uri = ContentUris.withAppendedId(ExpensesContract.Categories.CONTENT_URI, categoryId)

        // Defines a variable to contain the number of rows deleted
        val rowsDeleted: Int

        // Deletes the category that matches the selection criteria
        rowsDeleted = activity!!.contentResolver.delete(
                uri,  // the URI of the row to delete
                null,  // where clause
                null // where args
        )
        reloadCategoryList()
        showStatusMessage(resources.getString(R.string.category_deleted))
        return rowsDeleted
    }

    private fun deleteAssociatedExpenses(categoryId: Long): Int {
        val selection: String = ExpensesContract.ExpensesColumns.Companion.CATEGORY_ID + " = ?"
        val selectionArgs = arrayOf(categoryId.toString())
        return activity!!.contentResolver.delete(
                ExpensesContract.Expenses.CONTENT_URI,
                selection,
                selectionArgs
        )
    }

    private fun deleteCategory(categoryId: Long) {
        AlertDialog.Builder(activity!!)
                .setTitle(R.string.delete_category)
                .setMessage(R.string.delete_cat_dialog_msg)
                .setNeutralButton(android.R.string.cancel, null)
                .setPositiveButton(R.string.delete_string) { dialogInterface, i ->
                    val expenseRowsDeleted = deleteAssociatedExpenses(categoryId)
                    val statusMsg = resources.getQuantityString(
                            R.plurals.expenses_deleted_plurals_msg, expenseRowsDeleted, expenseRowsDeleted)
                    showStatusMessage(statusMsg)
                    deleteSingleCategory(categoryId)
                }
                .setIcon(R.drawable.ic_dialog_alert)
                .show()
    }

    private fun showStatusMessage(text: CharSequence) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    private fun prepareCategoryToCreate() {
        startActivity(Intent(activity, CategoryEditActivity::class.java))
    }

    private fun prepareCategoryToEdit(id: Long) {
        val intent = Intent(activity, CategoryEditActivity::class.java)
        intent.putExtra(CategoryEditFragment.Companion.EXTRA_EDIT_CATEGORY, id)
        startActivity(intent)
    }
}