package com.dailyexpense.expensetrack.fragment

import android.app.DatePickerDialog.OnDateSetListener
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.*
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.loader.app.LoaderManager
import androidx.loader.content.CursorLoader
import androidx.loader.content.Loader
import com.dailyexpense.expensetrack.R
import com.dailyexpense.expensetrack.activity.MainActivity
import com.dailyexpense.expensetrack.adapters.SectionExpenseAdapter
import com.dailyexpense.expensetrack.providers.ExpensesContract.Expenses
import com.dailyexpense.expensetrack.providers.ExpensesContract.ExpensesWithCategories
import com.dailyexpense.expensetrack.utils.Utils
import java.util.*

class ReportFragment : Fragment(), LoaderManager.LoaderCallbacks<Cursor>, PopupMenu.OnMenuItemClickListener {
    private var mExpensesListView: ListView? = null
    private var mAdapter: SectionExpenseAdapter? = null
    private var mProgressBar: View? = null
    private var mTotalValueTextView: TextView? = null
    private var mTotalCurrencyTextView: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_report, container, false)
        mExpensesListView = rootView.findViewById<View>(R.id.expenses_report_list_view) as ListView
        mProgressBar = rootView.findViewById(R.id.expenses_report_progress_bar)
        mTotalValueTextView = rootView.findViewById<View>(R.id.expenses_report_total_text_view) as TextView
        mTotalCurrencyTextView = rootView.findViewById<View>(R.id.expenses_report_total_currency_text_view) as TextView
        mExpensesListView!!.emptyView = rootView.findViewById(R.id.expenses_report_empty_list_view)
        mTotalValueTextView!!.text = Utils.formatToCurrency(0.0f)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mAdapter = SectionExpenseAdapter(activity)
        mExpensesListView!!.adapter = mAdapter
        initLoaders()
    }

    override fun onResume() {
        super.onResume()
        reloadReportData()
        reloadSharedPreferences()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_report, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.filter_expenses_menu_item -> {
                val menuItemView = activity!!.findViewById<View>(R.id.filter_expenses_menu_item)
                showPopupMenu(menuItemView)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun showPopupMenu(v: View?) {
        val popup = PopupMenu(activity!!, v!!)
        popup.setOnMenuItemClickListener(this@ReportFragment)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.report_filter_popup, popup.menu)
        popup.show()
    }

    /* from PopupMenu.OnMenuItemClickListener */
    override fun onMenuItemClick(item: MenuItem): Boolean {
        (activity as MainActivity?)!!.hideNavigationBar()
        return when (item.itemId) {
            R.id.today_filter_option -> {
                makeTodaysReport()
                true
            }
            R.id.week_filter_option -> {
                makeWeeklyReport()
                true
            }
            R.id.month_filter_option -> {
                makeMonthlyReport()
                true
            }
            R.id.date_filter_option -> {
                makeDateReport()
                true
            }
            R.id.range_filter_option -> {
                makeDateRangeReport()
                true
            }
            else -> false
        }
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        val reportType = args!!.getInt(REPORT_TYPE)
        val selectionArgs = args.getStringArray(SELECTION_ARGS)
        var uri: Uri? = null
        when (id) {
            SUM_LOADER_ID -> if (reportType == DATE_REPORT) {
                uri = ExpensesWithCategories.SUM_DATE_CONTENT_URI
            } else if (reportType == DATE_RANGE_REPORT) {
                uri = ExpensesWithCategories.SUM_DATE_RANGE_CONTENT_URI
            }
            LIST_LOADER_ID -> {
                mProgressBar!!.visibility = View.VISIBLE
                if (reportType == DATE_REPORT) {
                    uri = ExpensesWithCategories.DATE_CONTENT_URI
                } else if (reportType == DATE_RANGE_REPORT) {
                    uri = ExpensesWithCategories.DATE_RANGE_CONTENT_URI
                }
            }
        }
        return CursorLoader(activity!!,
                uri!!,
                null,
                null,
                selectionArgs,
                null
        )
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor) {
        when (loader.id) {
            SUM_LOADER_ID -> {
                val valueSumIndex = data.getColumnIndex(Expenses.VALUES_SUM)
                data.moveToFirst()
                val valueSum = data.getFloat(valueSumIndex)
                mTotalValueTextView!!.text = Utils.formatToCurrency(valueSum)
            }
            LIST_LOADER_ID -> {
                // Hide the progress bar
                mProgressBar!!.visibility = View.GONE
                // Update adapter's data
                mAdapter!!.swapCursor(data)
            }
        }
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {
        mAdapter!!.swapCursor(null)
    }

    private fun reloadSharedPreferences() {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)
        val prefCurrency = sharedPref.getString(SettingsFragment.Companion.KEY_PREF_CURRENCY, "")
        mTotalCurrencyTextView!!.text = prefCurrency
        mAdapter!!.setCurrency(prefCurrency)
    }

    private fun reloadReportData() {
        // Show the progress bar
        mProgressBar!!.visibility = View.VISIBLE
        // Today's report by default
        makeTodaysReport()
    }

    private fun makeTodaysReport() {
        val today = Utils.getDateString(Date())
        activity!!.title = getString(R.string.filter_todays_expenses)
        val selectionArgs = arrayOf(today)
        restartLoaders(DATE_REPORT, selectionArgs)
    }

    private fun makeWeeklyReport() {
        val calendar = Calendar.getInstance()
        val todayDate = Date()
        calendar.time = todayDate // Set today
        calendar.add(Calendar.DAY_OF_YEAR, -7) // Subtract 7 days from today
        val startDate = Utils.getDateString(calendar.time)
        val endDate = Utils.getDateString(todayDate)
        activity!!.title = getString(R.string.filter_weeks_expenses)
        val selectionArgs = arrayOf(startDate, endDate)
        restartLoaders(DATE_RANGE_REPORT, selectionArgs)
    }

    private fun makeMonthlyReport() {
        val calendar = Calendar.getInstance()
        calendar[Calendar.DAY_OF_MONTH] = 1 // Set the first day of month to 1
        val startDate = Utils.getDateString(calendar.time) // Get start of month
        val endDate = Utils.getDateString(Date())
        activity!!.title = getString(R.string.filter_months_expenses)
        val selectionArgs = arrayOf(startDate, endDate)
        restartLoaders(DATE_RANGE_REPORT, selectionArgs)
    }

    private fun makeDateReport() {
        val listener = OnDateSetListener { view, year, month, day ->
            val calendar = Calendar.getInstance()
            calendar[year, month] = day
            val dateString = Utils.getDateString(calendar.time)
            val systemFormatDateStr = Utils.getSystemFormatDateString(activity,
                    calendar.time)
            activity!!.title = getString(R.string.filter_date_expenses, systemFormatDateStr)
            val selectionArgs = arrayOf(dateString)
            restartLoaders(DATE_REPORT, selectionArgs)
        }
        val datePickerFragment: DatePickerFragment = DatePickerFragment.Companion.newInstance(listener)
        datePickerFragment.show(activity!!.supportFragmentManager, "date_picker")
    }

    private var startDate: Date? = null
    private fun makeDateRangeReport() {
        val endDateListener = OnDateSetListener { view, year, month, day ->
            val calendar = Calendar.getInstance()
            calendar[year, month] = day
            val startDateStr = Utils.getDateString(startDate)
            val endDateStr = Utils.getDateString(calendar.time)
            val sysFormatEndDateStr = Utils.getSystemFormatDateString(activity,
                    calendar.time)
            val sysFormatStartDateStr = Utils.getSystemFormatDateString(activity, startDate)
            activity!!.title = getString(R.string.filter_date_range_expenses,
                    sysFormatStartDateStr, sysFormatEndDateStr)
            val selectionArgs = arrayOf(startDateStr, endDateStr)
            restartLoaders(DATE_RANGE_REPORT, selectionArgs)
        }
        val startDateListener = OnDateSetListener { view, year, month, day ->
            val calendar = Calendar.getInstance()
            calendar[year, month] = day
            startDate = calendar.time
            val datePickerFragment: DatePickerFragment = DatePickerFragment.Companion.newInstance(endDateListener)
            datePickerFragment.show(activity!!.supportFragmentManager, "end_date_picker")
        }
        val datePickerFragment: DatePickerFragment = DatePickerFragment.Companion.newInstance(startDateListener)
        datePickerFragment.show(activity!!.supportFragmentManager, "start_date_picker")
    }

    private fun restartLoaders(reportType: Int, selectionArgs: Array<String>) {
        val args = createBundleArgs(reportType, selectionArgs)
        loaderManager.restartLoader(SUM_LOADER_ID, args, this)
        loaderManager.restartLoader(LIST_LOADER_ID, args, this)
    }

    private fun initLoaders() {
        // Retrieve today's date string
        val today = Utils.getDateString(Date())
        val selectionArgs = arrayOf(today)
        val args = createBundleArgs(DATE_REPORT, selectionArgs)

        // Initialize the CursorLoaders
        loaderManager.initLoader(SUM_LOADER_ID, args, this)
        loaderManager.initLoader(LIST_LOADER_ID, args, this)
    }

    private fun createBundleArgs(reportType: Int, selectionArgs: Array<String>): Bundle {
        val args = Bundle()
        args.putInt(REPORT_TYPE, reportType)
        args.putStringArray(SELECTION_ARGS, selectionArgs)
        return args
    }

    companion object {
        private const val SUM_LOADER_ID = 0
        private const val LIST_LOADER_ID = 1
        private const val REPORT_TYPE = "report_type"
        private const val SELECTION_ARGS = "selection_args"
        private const val DATE_REPORT = 10
        private const val DATE_RANGE_REPORT = 11
    }
}