package com.dailyexpense.expensetrack.fragment

import android.content.ContentUris
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.*
import android.view.ContextMenu.ContextMenuInfo
import android.widget.AdapterView.AdapterContextMenuInfo
import android.widget.AdapterView.OnItemClickListener
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.loader.app.LoaderManager
import androidx.loader.content.CursorLoader
import androidx.loader.content.Loader
import com.dailyexpense.expensetrack.R
import com.dailyexpense.expensetrack.activity.ExpenseEditActivity
import com.dailyexpense.expensetrack.adapters.SimpleExpenseAdapter
import com.dailyexpense.expensetrack.providers.ExpensesContract.Expenses
import com.dailyexpense.expensetrack.providers.ExpensesContract.ExpensesWithCategories
import com.dailyexpense.expensetrack.utils.Utils
import java.util.*

class TodayFragment : Fragment(), LoaderManager.LoaderCallbacks<Cursor> {
    private var mExpensesView: ListView? = null
    private var mProgressBar: View? = null
    private var mAdapter: SimpleExpenseAdapter? = null
    private var mTotalExpSumTextView: TextView? = null
    private var mTotalExpCurrencyTextView: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_today, container, false)
        mExpensesView = rootView.findViewById<View>(R.id.expenses_list_view) as ListView
        mProgressBar = rootView.findViewById(R.id.expenses_progress_bar)
        mTotalExpSumTextView = rootView.findViewById<View>(R.id.total_expense_sum_text_view) as TextView
        mTotalExpCurrencyTextView = rootView.findViewById<View>(R.id.total_expense_currency_text_view) as TextView
        mExpensesView!!.emptyView = rootView.findViewById(R.id.expenses_empty_list_view)
        mExpensesView!!.onItemClickListener = OnItemClickListener { parent, view, position, id -> prepareExpenseToEdit(id) }
        rootView.findViewById<View>(R.id.add_expense_button_if_empty_list).setOnClickListener { prepareExpenseToCreate() }
        mTotalExpSumTextView!!.text = Utils.formatToCurrency(0.0f)
        registerForContextMenu(mExpensesView!!)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // Set default values for preferences (settings) on startup
        PreferenceManager.setDefaultValues(activity, R.xml.preferences, false)
        mAdapter = SimpleExpenseAdapter(activity)
        mExpensesView!!.adapter = mAdapter

        // Initialize the CursorLoaders
        loaderManager.initLoader(SUM_LOADER_ID, null, this)
        loaderManager.initLoader(LIST_LOADER_ID, null, this)
    }

    override fun onResume() {
        super.onResume()
        reloadExpenseData()
        reloadSharedPreferences()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_today, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.new_expense_menu_item -> {
                prepareExpenseToCreate()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        activity!!.menuInflater.inflate(R.menu.expense_list_item_context, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as AdapterContextMenuInfo
        return when (item.itemId) {
            R.id.delete_expense_menu_item -> {
                deleteExpense(info.id)
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        var uri: Uri? = null
        when (id) {
            SUM_LOADER_ID -> uri = ExpensesWithCategories.SUM_DATE_CONTENT_URI
            LIST_LOADER_ID -> uri = ExpensesWithCategories.DATE_CONTENT_URI
        }

        // Retrieve today's date string
        val today = Utils.getDateString(Date())
        val selectionArgs = arrayOf(today)
        return CursorLoader(activity!!,
                uri!!,
                null,
                null,
                selectionArgs,
                null
        )
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor) {
        when (loader.id) {
            SUM_LOADER_ID -> {
                val valueSumIndex = data.getColumnIndex(Expenses.VALUES_SUM)
                data.moveToFirst()
                val valueSum = data.getFloat(valueSumIndex)
                mTotalExpSumTextView!!.text = Utils.formatToCurrency(valueSum)
            }
            LIST_LOADER_ID -> {
                // Hide the progress bar
                mProgressBar!!.visibility = View.GONE
                mAdapter!!.swapCursor(data)
            }
        }
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {
        when (loader.id) {
            SUM_LOADER_ID -> mTotalExpSumTextView!!.text = Utils.formatToCurrency(0.0f)
            LIST_LOADER_ID -> mAdapter!!.swapCursor(null)
        }
    }

    private fun reloadSharedPreferences() {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)
        val prefCurrency = sharedPref.getString(SettingsFragment.Companion.KEY_PREF_CURRENCY, "")
        mTotalExpCurrencyTextView!!.text = prefCurrency
        mAdapter!!.setCurrency(prefCurrency)
    }

    private fun reloadExpenseData() {
        // Show the progress bar
        mProgressBar!!.visibility = View.VISIBLE
        // Reload data by restarting the cursor loaders
        loaderManager.restartLoader(LIST_LOADER_ID, null, this)
        loaderManager.restartLoader(SUM_LOADER_ID, null, this)
    }

    private fun deleteSingleExpense(expenseId: Long): Int {
        val uri = ContentUris.withAppendedId(Expenses.CONTENT_URI, expenseId)

        // Defines a variable to contain the number of rows deleted
        val rowsDeleted: Int

        // Deletes the expense that matches the selection criteria
        rowsDeleted = activity!!.contentResolver.delete(
                uri,  // the URI of the row to delete
                null,  // where clause
                null // where args
        )
        showStatusMessage(resources.getString(R.string.expense_deleted))
        reloadExpenseData()
        return rowsDeleted
    }

    private fun deleteExpense(expenseId: Long) {
        AlertDialog.Builder(activity!!)
                .setTitle(R.string.delete_expense)
                .setMessage(R.string.delete_exp_dialog_msg)
                .setNeutralButton(android.R.string.cancel, null)
                .setPositiveButton(R.string.delete_string) { dialogInterface, i -> deleteSingleExpense(expenseId) }
                .show()
    }

    private fun showStatusMessage(text: CharSequence) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    private fun prepareExpenseToCreate() {
        startActivity(Intent(activity, ExpenseEditActivity::class.java))
    }

    private fun prepareExpenseToEdit(id: Long) {
        val intent = Intent(activity, ExpenseEditActivity::class.java)
        intent.putExtra(ExpenseEditFragment.Companion.EXTRA_EDIT_EXPENSE, id)
        startActivity(intent)
    }

    companion object {
        private const val SUM_LOADER_ID = 0
        private const val LIST_LOADER_ID = 1
    }
}