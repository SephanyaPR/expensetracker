package com.dailyexpense.expensetrack.fragment

import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.preference.*
import com.dailyexpense.expensetrack.R

class SettingsFragment : PreferenceFragmentCompat() {
    private val mSharedPrefChangeListener = OnSharedPreferenceChangeListener { sharedPrefs, key -> updateSinglePreference(findPreference(key), key) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences)
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        super.onActivityCreated(savedInstanceState)
        PreferenceManager.setDefaultValues(activity, R.xml.preferences, false)
    }



    override fun onCreateView(paramLayoutInflater: LayoutInflater, paramViewGroup: ViewGroup?, paramBundle: Bundle?): View? {
        return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle)
    }

    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences
                .registerOnSharedPreferenceChangeListener(mSharedPrefChangeListener)
        updatePreferences()
    }

    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences
                .unregisterOnSharedPreferenceChangeListener(mSharedPrefChangeListener)
    }

    private fun updatePreferences() {
        for (i in 0 until preferenceScreen.preferenceCount) {
            val pref = preferenceScreen.getPreference(i)
            if (pref is PreferenceGroup) {
                val prefGroup = pref
                for (j in 0 until prefGroup.preferenceCount) {
                    val singlePref = prefGroup.getPreference(j)
                    updateSinglePreference(singlePref, singlePref.key)
                }
            } else {
                updateSinglePreference(pref, pref.key)
            }
        }
    }

    private fun updateSinglePreference(pref: Preference?, key: String) {
        if (pref == null) return
        val sharedPrefs = preferenceManager.sharedPreferences
        val defaultValue = resources.getString(R.string.default_string)
        if (pref is ListPreference) {
            val listPref = pref
            listPref.summary = (listPref.entry.toString() + " ("
                    + sharedPrefs.getString(key, defaultValue) + ")")
            return
        }
        pref.summary = sharedPrefs.getString(key, defaultValue)
    }

    companion object {
        const val KEY_PREF_CURRENCY = "pref_currency"
    }
}