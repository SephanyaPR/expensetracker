package com.dailyexpense.expensetrack.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import com.dailyexpense.expensetrack.R
import com.dailyexpense.expensetrack.providers.ExpensesContract.CategoriesColumns
import com.dailyexpense.expensetrack.providers.ExpensesContract.ExpensesColumns

class ExpenseDbHelper(private val mContext: Context?) : SQLiteOpenHelper(mContext, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CategoriesTable.CREATE_TABLE_QUERY)
        // Fill the table with predefined values
        CategoriesTable.fillTable(db, mContext)
        db.execSQL(ExpensesTable.CREATE_TABLE_QUERY)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        /* Temporary (dummy) upgrade policy */
        db.execSQL(ExpensesTable.DELETE_TABLE_QUERY)
        db.execSQL(CategoriesTable.DELETE_TABLE_QUERY)
        onCreate(db)
    }

    private object CategoriesTable {
        val CREATE_TABLE_QUERY = "CREATE TABLE " + CATEGORIES_TABLE_NAME + " (" +
                BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CategoriesColumns.Companion.NAME + " TEXT NOT NULL);"
        const val DELETE_TABLE_QUERY = "DROP TABLE IF EXISTS $CATEGORIES_TABLE_NAME;"
        fun fillTable(db: SQLiteDatabase, ctx: Context?) {
            val predefinedNames = ctx!!.resources.getStringArray(R.array.predefined_categories)
            val values = ContentValues()
            for (name in predefinedNames) {
                values.put(CategoriesColumns.Companion.NAME, name)
                db.insert(CATEGORIES_TABLE_NAME, null, values)
            }
        }
    }

    private object ExpensesTable {
        val CREATE_TABLE_QUERY = "CREATE TABLE " + EXPENSES_TABLE_NAME + " (" +
                BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                ExpensesColumns.Companion.VALUE + " FLOAT NOT NULL, " +
                ExpensesColumns.Companion.DATE + " DATE NOT NULL, " +
                ExpensesColumns.Companion.CATEGORY_ID + " INTEGER NOT NULL);"
        const val DELETE_TABLE_QUERY = "DROP TABLE IF EXISTS $EXPENSES_TABLE_NAME;"
    }

    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "expense_tracer.db"
        const val CATEGORIES_TABLE_NAME = "categories"
        const val EXPENSES_TABLE_NAME = "expenses"
    }

}